package com.example.tubes2_p3b;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.example.tubes2_p3b.databinding.HistoryPageBinding;

import java.util.List;

public class HistoryPageFragment extends Fragment implements View.OnClickListener, FragmentResultListener, HistoryPresenter.HistoryInterface {
    HistoryPageBinding binding;
    HistoryAdapter adapter;
    ListView lv;
    History hist;
//    OrdersTask ordersTask;
//    Orders orders;
    String token;
    int limit = 10;
    int offset = 0;
    int test;
    String source;
    HistoryPresenter historyPresenter;


    public static HistoryPageFragment newInstance(String title) {
        HistoryPageFragment fragment = new HistoryPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = HistoryPageBinding.inflate(inflater,container,false);
        View view = inflater.inflate(R.layout.history_page, container, false);
        Log.d("tokenStatic", LoginPageFragment.token);
        this.historyPresenter = new HistoryPresenter(this.getContext(), this);
        this.historyPresenter.executeHistory(this.limit, this.offset, LoginPageFragment.token);
        //ongoing
//        String[] routeOn = {"Bandung - Jakarta"};
//        String[] dateOn = {"01-01-2022"};
//
//        this.adapter = new HistoryAdapter(getActivity());
//        for(int a = 0; a <routeOn.length; a++) {
//            hist = new History(routeOn[a], dateOn[a]);
//            this.adapter.addLine(hist);
//        }
//
//        this.lv = (ListView) view.findViewById(R.id.lv_ongoing);
//        this.lv.setAdapter(this.adapter);

        //completed
        String[] routeComp = {"Bandung - Jakarta", "Jakarta - Bandung"};
        String[] dateComp = {"01-11-2020", "02-12-2020"};

        this.adapter = new HistoryAdapter(getActivity());
//        for(int a = 0; a <routeComp.length; a++) {
//            hist = new History(routeComp[a], dateComp[a]);
//            this.adapter.addLine(hist);
//        }

        this.lv = (ListView) view.findViewById(R.id.lv_completed);
        this.lv.setAdapter(this.adapter);
//        ambilDataOrders();

//        this.getParentFragmentManager().setFragmentResultListener("token", this, this);
//        this.ordersTask = new OrdersTask(this.getActivity(), this);
//
//        this.ordersTask.executeOrders(limit, offset, this.token);

//        for(int i=0; i<limit; i++) {
//            String route = this.orders.getSource() + " - " + this.orders.getDestination();
//            String date = this.orders.getCourse_datetime().substring(12);
//
//            History currentHist = new History(route, date);
//            this.adapter.addLine(currentHist);
//        }
        //Log.d("testHistory", String.valueOf(this.test));
        this.getParentFragmentManager().setFragmentResultListener("cobaSource", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                Log.d("SourceTestLAgi", result.getString("cobaSource"));
            }
        });


        return view;
    }

//    private void ambilDataOrders() {
//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        String base_url = "https://devel.loconode.com/pppb/v1/orders?";

//        String URL = base_url + "limit=" + limit + "&offset=" + offset;
//
//        JSONObject jsonObject = new JSONObject();
//        final String requestBody = jsonObject.toString();
//
//        StringRequest stringRequest = new StringRequest(
//                Request.Method.GET, URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject obj = new JSONObject(response.toString());
//                    JSONArray payload = obj.getJSONArray("payload");
//
//                    for (int i = 0; i < payload.length(); i++) {
//                        JSONObject order = payload.getJSONObject(i);
//                        String from = order.getString("source");
//                        String to = order.getString("destination");
//                        String date = order.getString("course_datetime");
//
//                        History currentHist = new History(from + " - " + to, date.substring(12));
//                        adapter.addLine(currentHist);
//                    }
//                    Toast.makeText(getContext(), "Berhasil ambil data history", Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), "Gagal ambil data history" + error, Toast.LENGTH_SHORT).show();
//            }
//        }
//        );
//        queue.add(stringRequest);
//    }

    @Override
    public void onClick(View view) { //6

    }

    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        this.token = result.getString("token");
    }

    @Override
    public void callback(List<HistoryOrder> history) {
        //this.source = history.get(0).getSource();

        String rute[] = new String[history.size()];
        for(int i = 0; i < history.size(); i++){
            rute[i] = history.get(i).getSource() + " - " + history.get(i).getDestination();
        }

        String[] tanggal = new String[history.size()];
        for(int i = 0; i < history.size(); i++){
            tanggal[i] = history.get(i).getOrder_datetime();
        }
//        hist = new History(source, source);
//        this.adapter.addLine(hist);
//
        for(int i = 0; i < history.size(); i++) {
            hist = new History(rute[i], tanggal[i]);
            this.adapter.addLine(hist);
        }
    }

}
