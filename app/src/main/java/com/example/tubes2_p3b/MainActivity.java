package com.example.tubes2_p3b;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;

import com.example.tubes2_p3b.databinding.ActivityMainBinding;



public class MainActivity extends AppCompatActivity implements FragmentResultListener {
    protected ActivityMainBinding binding;
    protected LandingPageFragment lpf;
    protected LoginPageFragment logpf;
    protected HistoryPageFragment historyFrag;
    protected FragmentManager fragmentManager;
    protected OrderPageFragment orderFrag;
    protected OngoingPageFragment ongoingFrag;
    protected SeatPageLargeFragment largeFrag;
    protected SeatPageSmallFragment smallFrag;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(this.binding.getRoot());
        this.lpf = LandingPageFragment.newInstance("Landing Fragment");
        this.logpf=LoginPageFragment.newInstance("Login Fragment");
        this.historyFrag = HistoryPageFragment.newInstance("History Fragment");
        this.orderFrag = OrderPageFragment.newInstance("Order Fragment");
        this.ongoingFrag = OngoingPageFragment.newInstance("Ongoing Fragment");
        this.largeFrag = SeatPageLargeFragment.newInstance("Seat Page Large Fragment");
        this.smallFrag = SeatPageSmallFragment.newInstance("Seat Page Small Fragment");

        this.setSupportActionBar(this.binding.toolbar);

        ActionBarDrawerToggle abdt = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.openDrawer, R.string.closeDrawer);
        this.binding.drawerLayout.addDrawerListener(abdt);
        abdt.syncState();

        this.fragmentManager = this.getSupportFragmentManager();

        this.fragmentManager.beginTransaction().add(R.id.fragment_container, logpf).addToBackStack(null).commit();

        this.fragmentManager.setFragmentResultListener("changePage", this, this);
        this.fragmentManager.setFragmentResultListener("closeApplication", this, this);

    }

    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        int page = result.getInt("page");
        boolean exit = result.getBoolean("exit");
        if (exit == true) {
            closeApplication(exit);
        }
        changePage(page);
    }


    public void changePage(int page) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (page == 1) { //login
            if (this.logpf.isAdded()) {
                ft.show(this.logpf);
            } else {
                ft.add(R.id.fragment_container, this.logpf);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if (this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if (this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }
        else if(page == 2) { //landing
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.lpf.isAdded()) {
                ft.show(this.lpf);
            } else {
                ft.add(R.id.fragment_container, this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if (this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if(this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if(this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }
        else if(page == 3) { //history
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.historyFrag.isAdded()) {
                ft.show(this.historyFrag);
            } else {
                ft.add(R.id.fragment_container, this.historyFrag).addToBackStack(null);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if(this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if(this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }
        else if(page == 4) { //order
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.orderFrag.isAdded()) {
                ft.show(this.orderFrag);
            } else {
                ft.add(R.id.fragment_container, this.orderFrag).addToBackStack(null);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if(this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if(this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }
        else if(page == 5) { //seat small
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.smallFrag.isAdded()) {
                ft.show(this.smallFrag);
            } else {
                ft.add(R.id.fragment_container, this.smallFrag).addToBackStack(null);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if(this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if(this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if(this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }

        else if(page == 7) { //ongoing
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.ongoingFrag.isAdded()) {
                ft.show(this.ongoingFrag);
            } else {
                ft.add(R.id.fragment_container, this.ongoingFrag).addToBackStack(null);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if(this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if(this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
            if (this.largeFrag.isAdded()) {
                ft.hide(this.largeFrag);
            }
        }
        else if(page == 8) { //seat large
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            if (this.largeFrag.isAdded()) {
                ft.show(this.largeFrag);
            } else {
                ft.add(R.id.fragment_container, this.largeFrag).addToBackStack(null);
            }
            if (this.lpf.isAdded()) {
                ft.hide(this.lpf);
            }
            if (this.logpf.isAdded()) {
                ft.hide(this.logpf);
            }
            if(this.orderFrag.isAdded()) {
                ft.hide(this.orderFrag);
            }
            if(this.historyFrag.isAdded()) {
                ft.hide(this.historyFrag);
            }
            if(this.ongoingFrag.isAdded()) {
                ft.hide(this.ongoingFrag);
            }
            if (this.smallFrag.isAdded()) {
                ft.hide(this.smallFrag);
            }
        }
        ft.commit();
    }


    private void closeApplication(boolean exit) {
        this.moveTaskToBack(true);
        this.finish();
    }


}