package com.example.tubes2_p3b;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RoutesPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/routes";
    private Context context;
    private Gson gson;
    private PostRoutesInterface IMainActivity;
    private JSONObject objJSON;

    public RoutesPresenter(Context context, PostRoutesInterface IMainActivity) {
        this.context = context;
        this.IMainActivity = IMainActivity;
    }

    public void executeRoutes(String source, String destination, String token) {
        gson = new Gson();
        InputRoutes input = new InputRoutes(source, destination);
        try {
            objJSON = new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("tokenvolley", token);
        this.callVolley(objJSON, token);

        //  this.callVolley(expr,precision);
    }

    private void callVolley(JSONObject toJson, String token) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ResponRoute", response.toString());
                        processResult(response.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers= new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(jsonRequest);
    }

    private void processResult(String json) {
        ResultRoutes result = gson.fromJson(json, ResultRoutes.class);
        List<Routes> rute = result.getPayload();
        this.IMainActivity.callback(rute);
    }

    interface PostRoutesInterface {
        public void callback(List<Routes> routes);
    }
}