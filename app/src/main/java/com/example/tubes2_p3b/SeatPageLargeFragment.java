package com.example.tubes2_p3b;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.example.tubes2_p3b.databinding.SeatPageLargeBinding;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Arrays;
import java.util.List;

public class SeatPageLargeFragment extends Fragment implements View.OnTouchListener, OrderPresenter.OrderInterface, FragmentResultListener{
    SeatPageLargeBinding binding;
    private Canvas mCanvas;
    private Bitmap mBitmap;
    private GestureDetector mDetector;
    private Paint paintKosong;
    private Paint paintTeisi;
    private Paint paintDipilih;
    private Paint strokePaint;
    private Paint strokePaintDipilih;
    private Paint paintDiisi;
    private Paint strokePaintDiisi;
    private Paint paintText;
    private int[] ditekan = new int[10];
    private int[] seats;
    private OrderPresenter orderPresenter;
    private String token;
    private String course_id;

    public SeatPageLargeFragment() { }

    public static SeatPageLargeFragment newInstance(String title) {
        SeatPageLargeFragment fragment = new SeatPageLargeFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = SeatPageLargeBinding.inflate(this.getLayoutInflater());
        this.mDetector = new GestureDetector(this.getContext(), new MyCustomGestureListener());
        this.binding.ivSeat.setOnTouchListener(this);



        getParentFragmentManager().setFragmentResultListener("seats", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                seats = result.getIntArray("seats");
                initiateCanvas(kursiTersisi(seats));
            }
        });
        this.orderPresenter = new OrderPresenter(this.getActivity(), this);
        this.getParentFragmentManager().setFragmentResultListener("course_id", this, this);
        this.getParentFragmentManager().setFragmentResultListener("tokenOrder", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String token2 = result.getString("tokenOrder");
                binding.btnNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v == binding.btnNext) {
                            String seatDipesan ="";
                            for (int i = 0; i < ditekan.length; i++) {
                                if (ditekan[i] == 1) {
                                    seatDipesan += Integer.toString(i + 1) + ",";
                                }
                            }
                            seatDipesan = seatDipesan.substring(0, seatDipesan.length() - 1);
                            Log.d("seatDipesan", seatDipesan);
                            Log.d("tokenOrder", token2);

                            orderPresenter.executeOrder(course_id, seatDipesan, token2);

                            Bundle result = new Bundle();
                            result.putInt("page", 7);
                            getParentFragmentManager().setFragmentResult("changePage", result);
                        }
                    }
                });
            }
        });

        return this.binding.getRoot();
    }


    private static int[] kursiTersisi(int[] seats){
        int[] kursi = {0,0,0,0,0,0,0,0,0,0};
        if(seats == null){
            return kursi;
        }else{
            for (int i = 0; i < seats.length; i++){
                kursi[seats[i]-1] = 1;
            }
            return kursi;
        }
    }

    public void initiateCanvas(int[] kursiTerisi){
        // 1. Create Bitmap
        this.mBitmap = Bitmap.createBitmap(350, 500, Bitmap.Config.ARGB_8888);
        // 2. Associate the bitmap to the ImageView.
        this.binding.ivSeat.setImageBitmap(mBitmap);
        // 3. Create a Canvas with the bitmap.
        this.mCanvas = new Canvas(mBitmap);
        // background canvas
        int mColorBackground = ResourcesCompat.getColor(getResources(), R.color.canvasSeat, null);
        this.mCanvas.drawColor(mColorBackground);

        Log.d("seatsTest", String.valueOf(seats));

        //kosong
        this.paintKosong = new Paint();
        paintKosong.setColor(this.getResources().getColor(R.color.black));
        this.paintKosong.setStyle(Paint.Style.FILL);

        this.strokePaint = new Paint();
        strokePaint.setColor(this.getResources().getColor(R.color.tersedia ));
        this.paintKosong.setStyle(Paint.Style.STROKE);
        this.strokePaint.setStrokeWidth(5);

        this.paintText = new Paint();
        paintText.setColor(this.getResources().getColor(R.color.black ));
        this.paintText.setTextSize(20);
        //diisi
        this.paintDiisi = new Paint();
        paintDiisi.setColor(this.getResources().getColor(R.color.black));
        this.paintDiisi.setStyle(Paint.Style.FILL);


        strokePaintDiisi = new Paint();
        strokePaintDiisi.setColor(getResources().getColor(R.color.diisi));
        paintDiisi.setStyle(Paint.Style.STROKE);
        strokePaintDiisi.setStrokeWidth(5);

        paintDipilih = new Paint();
        paintDipilih.setColor(getResources().getColor(R.color.black));
        paintDipilih.setStyle(Paint.Style.FILL);

        strokePaintDipilih = new Paint();
        strokePaintDipilih.setColor(getResources().getColor(R.color.midnight_blue));
        paintDipilih.setStyle(Paint.Style.STROKE);
        strokePaintDipilih.setStrokeWidth(5);

        paintText = new Paint();
        paintText.setColor(getResources().getColor(R.color.black ));
        paintText.setTextSize(20);


        //supir
        mCanvas.drawRect(240,110,290,160,paintKosong);
        mCanvas.drawRect(241,111,289,159,strokePaint);
        mCanvas.drawText("S", 260, 143, paintText);


        if(kursiTerisi[0] == 0){
            //1
            mCanvas.drawRect(60,110,110,160,paintKosong);
            mCanvas.drawRect(61,111,109,159,strokePaint);
            mCanvas.drawText("1", 80, 143, paintText);
        }else{
            mCanvas.drawRect(60,110,110,160,paintDiisi);
            mCanvas.drawRect(61,111,109,159,strokePaintDiisi);
            mCanvas.drawText("1", 80, 143, paintText);
        }
        if (kursiTerisi[1] == 0){
            //2
            mCanvas.drawRect(60,200,110,250,paintKosong);
            mCanvas.drawRect(61,201,109,249,strokePaint);
            mCanvas.drawText("2", 80, 230, paintText);
        }else{
            mCanvas.drawRect(60,200,110,250,paintDiisi);
            mCanvas.drawRect(61,201,109,249,strokePaintDiisi);
            mCanvas.drawText("2", 80, 230, paintText);
        }
        if (kursiTerisi[2] == 0){
            //3
            mCanvas.drawRect(150,200,200,250,paintKosong);
            mCanvas.drawRect(151,201,199,249,strokePaint);
            mCanvas.drawText("3", 170, 230, paintText);
        }else{
            mCanvas.drawRect(150,200,200,250,paintDiisi);
            mCanvas.drawRect(151,201,199,249,strokePaintDiisi);
            mCanvas.drawText("3", 170, 230, paintText);
        }
        if (kursiTerisi[3] == 0){
            //4
            mCanvas.drawRect(240,200,290,250,paintKosong);
            mCanvas.drawRect(241,201,289,249,strokePaint);
            mCanvas.drawText("4", 260, 230, paintText);
        }else{
            mCanvas.drawRect(240,200,290,250,paintDiisi);
            mCanvas.drawRect(241,201,289,249,strokePaintDiisi);
            mCanvas.drawText("4", 260, 230, paintText);
        }
        if (kursiTerisi[4] == 0){
            //5
            mCanvas.drawRect(60,290,110,340,paintKosong);
            mCanvas.drawRect(61,291,109,339,strokePaint);
            mCanvas.drawText("5", 80, 320, paintText);
        }else{
            mCanvas.drawRect(60,290,110,340,paintDiisi);
            mCanvas.drawRect(61,291,109,339,strokePaintDiisi);
            mCanvas.drawText("5", 80, 320, paintText);
        }
        if (kursiTerisi[5] == 0){
            //6
            mCanvas.drawRect(150,290,200,340,paintKosong);
            mCanvas.drawRect(151,291,199,339,strokePaint);
            mCanvas.drawText("6", 170, 320, paintText);
        }else{
            mCanvas.drawRect(150,290,200,340,paintDiisi);
            mCanvas.drawRect(151,291,199,339,strokePaintDiisi);
            mCanvas.drawText("6", 170, 320, paintText);
        }
        if (kursiTerisi[6] == 0){
            //7
            mCanvas.drawRect(240,290,290,340,paintKosong);
            mCanvas.drawRect(241,291,289,339,strokePaint);
            mCanvas.drawText("7", 260, 320, paintText);
        }else{
            mCanvas.drawRect(240,290,290,340,paintDiisi);
            mCanvas.drawRect(241,291,289,339,strokePaintDiisi);
            mCanvas.drawText("7", 260, 320, paintText);
        }
        if (kursiTerisi[7] == 0){
            //8
            mCanvas.drawRect(60,380,110,430,paintKosong);
            mCanvas.drawRect(61,381,109,429,strokePaint);
            mCanvas.drawText("8", 80, 410, paintText);
        }else{
            mCanvas.drawRect(60,380,110,430,paintDiisi);
            mCanvas.drawRect(61,381,109,429,strokePaintDiisi);
            mCanvas.drawText("8", 80, 410, paintText);
        }
        if (kursiTerisi[8] == 0){
            //9
            mCanvas.drawRect(150,380,200,430,paintKosong);
            mCanvas.drawRect(151,381,199,429,strokePaint);
            mCanvas.drawText("9", 170, 410, paintText);
        }else{
            mCanvas.drawRect(150,380,200,430,paintDiisi);
            mCanvas.drawRect(151,381,199,429,strokePaintDiisi);
            mCanvas.drawText("9", 170, 410, paintText);
        }
        if (kursiTerisi[9] == 0){
            //10
            mCanvas.drawRect(240,380,290,430,paintKosong);
            mCanvas.drawRect(241,381,289,429,strokePaint);
            mCanvas.drawText("10", 255, 410, paintText);
        }else {
            mCanvas.drawRect(240,380,290,430,paintDiisi);
            mCanvas.drawRect(241,381,289,429,strokePaintDiisi);
            mCanvas.drawText("10", 255, 410, paintText);
        }
        Log.d("test", "test");
    }

    @Override
    public boolean onTouch(View view, MotionEvent e) {
        this.mDetector.onTouchEvent(e);
        return true;
    }

    @Override
    public void callbackOrder(List<Order> order) {

    }

    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        this.course_id = result.getString("course_id");
        this.token = result.getString("tokenOrder");
    }




    private class MyCustomGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            Log.d("X=", String.valueOf(e.getX()));
            Log.d("Y=", String.valueOf(e.getY()));

            float x = e.getX()/3;
            float y = e.getY()/3;

            int[] kursi = SeatPageLargeFragment.kursiTersisi(seats);
            if(x >= 60 && x <= 100 && y >= 100 && y <= 143){//1
                if (ditekan[0] == 0 && kursi[0] == 1){
                    mCanvas.drawRect(60,110,110,160,paintDiisi);
                    mCanvas.drawRect(61,111,109,159,strokePaintDiisi);
                    mCanvas.drawText("1", 80, 143, paintText);
                }else if (ditekan[0] == 0 && kursi[0] == 0) {
                    mCanvas.drawRect(60, 110, 110, 160, paintDipilih);
                    mCanvas.drawRect(61, 111, 109, 159, strokePaintDipilih);
                    mCanvas.drawText("1", 80, 143, paintText);
                    ditekan[0] = 1;
                }else{
                    mCanvas.drawRect(60, 110, 110, 160, paintKosong);
                    mCanvas.drawRect(61, 111, 109, 159, strokePaint);
                    mCanvas.drawText("1", 80, 143, paintText);
                    ditekan[0] = 0;
                }
            }else if (x >= 60 && x <= 100 && y >= 180 && y <= 230){//2
                if (ditekan[1] == 0 && kursi[1] == 1){
                    mCanvas.drawRect(60,200,110,250,paintDiisi);
                    mCanvas.drawRect(61,201,109,249,strokePaintDiisi);
                    mCanvas.drawText("2", 80, 230, paintText);
                }else if (ditekan[1] == 0 && kursi[1] == 0){
                    mCanvas.drawRect(60,200,110,250,paintDipilih);
                    mCanvas.drawRect(61,201,109,249,strokePaintDipilih);
                    mCanvas.drawText("2", 80, 230, paintText);
                    ditekan[1] = 1;
                }else{
                    mCanvas.drawRect(60,200,110,250,paintKosong);
                    mCanvas.drawRect(61,201,109,249,strokePaint);
                    mCanvas.drawText("2", 80, 230, paintText);
                    ditekan[1] = 0;
                }
            }else if (x >= 150 && x <= 200 && y >= 180 && y <= 230){//3
                if (ditekan[2] == 0 && kursi[2] == 1){
                    mCanvas.drawRect(150,200,200,250,paintDiisi);
                    mCanvas.drawRect(151,201,199,249,strokePaintDiisi);
                    mCanvas.drawText("3", 170, 230, paintText);
                }else if (ditekan[2] == 0 && kursi[2] == 0) {
                    mCanvas.drawRect(150,200,200,250,paintDipilih);
                    mCanvas.drawRect(151,201,199,249,strokePaintDipilih);
                    mCanvas.drawText("3", 170, 230, paintText);
                    ditekan[2] = 1;
                }else{
                    mCanvas.drawRect(150,200,200,250,paintKosong);
                    mCanvas.drawRect(151,201,199,249,strokePaint);
                    mCanvas.drawText("3", 170, 230, paintText);
                    ditekan[2] = 0;
                }
            }else if (x >= 240 && x <= 290 && y >= 180 && y <= 230){//4
                if (ditekan[3] == 0 && kursi[3] == 1){
                    mCanvas.drawRect(240,200,290,250,paintDiisi);
                    mCanvas.drawRect(241,201,289,249,strokePaintDiisi);
                    mCanvas.drawText("4", 260, 230, paintText);
                }else if (ditekan[3] == 0 && kursi[3] == 0) {
                    mCanvas.drawRect(240,200,290,250,paintDipilih);
                    mCanvas.drawRect(241,201,289,249,strokePaintDipilih);
                    mCanvas.drawText("4", 260, 230, paintText);
                    ditekan[3] = 1;
                }else if(ditekan[3] == 1 && kursi[3] == 0){
                    mCanvas.drawRect(240,200,290,250,paintKosong);
                    mCanvas.drawRect(241,201,289,249,strokePaint);
                    mCanvas.drawText("4", 260, 230, paintText);
                    ditekan[3] = 0;
                }
            }else if (x >= 60 && x <= 110 && y >= 270 && y <= 320){//5
                if (ditekan[4] == 0 && kursi[4] == 1){
                    mCanvas.drawRect(60,290,110,340,paintDiisi);
                    mCanvas.drawRect(61,291,109,339,strokePaintDiisi);
                    mCanvas.drawText("5", 80, 320, paintText);
                }else if (ditekan[4] == 0 && kursi[4] == 0) {
                    mCanvas.drawRect(60,290,110,340,paintDipilih);
                    mCanvas.drawRect(61,291,109,339,strokePaintDipilih);
                    mCanvas.drawText("5", 80, 320, paintText);
                    ditekan[4] = 1;
                }else{
                    mCanvas.drawRect(60,290,110,340,paintKosong);
                    mCanvas.drawRect(61,291,109,339,strokePaint);
                    mCanvas.drawText("5", 80, 320, paintText);
                    ditekan[4] = 0;
                }
            }else if (x >= 150 && x <= 200 && y >= 270 && y <= 320){//6
                if (ditekan[5] == 0 && kursi[5] == 1){
                    mCanvas.drawRect(150,290,200,340,paintDiisi);
                    mCanvas.drawRect(151,291,199,339,strokePaintDiisi);
                    mCanvas.drawText("6", 170, 320, paintText);
                }else if (ditekan[5] == 0 && kursi[5] == 0) {
                    mCanvas.drawRect(150,290,200,340,paintDipilih);
                    mCanvas.drawRect(151,291,199,339,strokePaintDipilih);
                    mCanvas.drawText("6", 170, 320, paintText);
                    ditekan[5] = 1;
                }else{
                    mCanvas.drawRect(150,290,200,340,paintKosong);
                    mCanvas.drawRect(151,291,199,339,strokePaint);
                    mCanvas.drawText("6", 170, 320, paintText);
                    ditekan[5] = 0;
                }
            }else if (x >= 240 && x <= 290 && y >= 270 && y <= 320){//7
                if (ditekan[6] == 0 && kursi[6] == 1){
                    mCanvas.drawRect(240,290,290,340,paintDiisi);
                    mCanvas.drawRect(241,291,289,339,strokePaintDiisi);
                    mCanvas.drawText("7", 260, 320, paintText);
                }else if (ditekan[6] == 0 && kursi[6] == 0) {
                    mCanvas.drawRect(240,290,290,340,paintDipilih);
                    mCanvas.drawRect(241,291,289,339,strokePaintDipilih);
                    mCanvas.drawText("7", 260, 320, paintText);
                    ditekan[6] = 1;
                }else{
                    mCanvas.drawRect(240,290,290,340,paintKosong);
                    mCanvas.drawRect(241,291,289,339,strokePaint);
                    mCanvas.drawText("7", 260, 320, paintText);
                    ditekan[6] = 0;
                }
            }else if (x >= 60 && x <= 110 && y >= 350 && y <= 400){//8
                if (ditekan[7] == 0 && kursi[7] == 1){
                    mCanvas.drawRect(60,380,110,430,paintDiisi);
                    mCanvas.drawRect(61,381,109,429,strokePaintDiisi);
                    mCanvas.drawText("8", 80, 410, paintText);
                }else if (ditekan[7] == 0 && kursi[7] == 0) {
                    mCanvas.drawRect(60,380,110,430,paintDipilih);
                    mCanvas.drawRect(61,381,109,429,strokePaintDipilih);
                    mCanvas.drawText("8", 80, 410, paintText);
                    ditekan[7] = 1;
                }else{
                    mCanvas.drawRect(60,380,110,430,paintKosong);
                    mCanvas.drawRect(61,381,109,429,strokePaint);
                    mCanvas.drawText("8", 80, 410, paintText);
                    ditekan[7] = 0;
                }
            }else if (x >= 150 && x <= 200 && y >= 350 && y <= 400){//9
                if (ditekan[8] == 0 && kursi[8] == 1){
                    mCanvas.drawRect(150,380,200,430,paintDiisi);
                    mCanvas.drawRect(151,381,199,429,strokePaintDiisi);
                    mCanvas.drawText("9", 170, 410, paintText);
                }else if (ditekan[8] == 0 && kursi[8] == 0) {
                    mCanvas.drawRect(150,380,200,430,paintDipilih);
                    mCanvas.drawRect(151,381,199,429,strokePaintDipilih);
                    mCanvas.drawText("9", 170, 410, paintText);
                    ditekan[8] = 1;
                }else{
                    mCanvas.drawRect(150,380,200,430,paintKosong);
                    mCanvas.drawRect(151,381,199,429,strokePaint);
                    mCanvas.drawText("9", 170, 410, paintText);
                    ditekan[8] = 0;
                }
            }else if (x >= 240 && x <= 290 && y >= 350 && y <= 400) {//10
                if (ditekan[9] == 0 && kursi[9] == 1){
                    mCanvas.drawRect(240,380,290,430,paintKosong);
                    mCanvas.drawRect(241,381,289,429,strokePaint);
                    mCanvas.drawText("10", 255, 410, paintText);
                }else if (ditekan[9] == 0 && kursi[9] == 0) {
                    mCanvas.drawRect(240,380,290,430,paintDipilih);
                    mCanvas.drawRect(241,381,289,429,strokePaintDipilih);
                    mCanvas.drawText("10", 255, 410, paintText);
                    ditekan[9] = 1;
                } else {
                    mCanvas.drawRect(240,380,290,430,paintKosong);
                    mCanvas.drawRect(241,381,289,429,strokePaint);
                    mCanvas.drawText("10", 255, 410, paintText);
                    ditekan[9] = 0;
                }

            }
            Log.d("testTekan", Arrays.toString(ditekan));
            //Log.d("ditekan", Arrays.toString(ditekan));
            Bundle result = new Bundle();
            result.putIntArray("dibeli", ditekan);

            int count = 0;
            for(int i = 0; i < ditekan.length; i++){
                if(ditekan[i] == 1){
                    count++;
                }
            }
            result.putInt("total", count*OrderPageFragment.fee);
            getParentFragmentManager().setFragmentResult("dibeli",result);
            getParentFragmentManager().setFragmentResult("total",result);

            binding.ivSeat.invalidate();
            return true;
        }
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            return true;
        }
        @Override
        public void onLongPress(MotionEvent e) {

        }
    }
}
