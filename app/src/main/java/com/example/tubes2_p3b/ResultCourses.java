package com.example.tubes2_p3b;

import java.util.List;

public class ResultCourses {
    private List<Courses> payload;

    public ResultCourses(List<Courses> courses){
        this.payload = courses;
    }

    public List<Courses> getCourses() {
        return payload;
    }

    public void setCourses(List<Courses> courses) {
        this.payload = courses;
    }

}

class Courses{
    String course_id;
    String source;
    String destination;
    String datetime;
    String vehicle;
    String num_seats;
    int[] seats;
    int fee;

    public Courses(String course_id, String source, String destination,String datetime, String vehicle, String num_seats, int[] seats, int fee){
        this.course_id = course_id;
        this.source = source;
        this.destination = destination;
        this.datetime = datetime;
        this.vehicle = vehicle;
        this.num_seats = num_seats;
        this.seats = seats;
        this.fee = fee;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public void setNum_seats(String num_seats) {
        this.num_seats = num_seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public String getCourse_id() {
        return course_id;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public String getNum_seats() {
        return num_seats;
    }

    public int[] getSeats() {
        return seats;
    }

    public int getFee() {
        return fee;
    }
}
