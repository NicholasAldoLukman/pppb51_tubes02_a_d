package com.example.tubes2_p3b;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.tubes2_p3b.databinding.LoginPageBinding;
import com.google.gson.Gson;

public class LoginPageFragment extends Fragment implements View.OnClickListener, LoginPresenter.PostLoginInterface {
    protected LoginPageBinding binding;
    private Gson gson;
    private LoginPresenter loginPresenter;
    static String token;

    public static LoginPageFragment newInstance(String title) {
        LoginPageFragment fragment = new LoginPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = LoginPageBinding.inflate(inflater, container, false);
        this.binding.btnLogin.setOnClickListener(this);
        View view = this.binding.getRoot();
        this.loginPresenter = new LoginPresenter(this.getActivity(),this);
        token = null;
        return view;

    }

    @Override
    public void onClick(View v) { //1
        this.gson = new Gson();
        String username = this.binding.etUsername.getText().toString();
        String password = this.binding.etPassword.getText().toString();
        loginPresenter.execute(username,password);
    }

    @Override
    public void callback(String token, String pesan) {
        Bundle result = new Bundle();
        result.putInt("page", 2);
        result.putString("token", token);
        Log.d("token", token);
        this.token = token;
        this.getParentFragmentManager().setFragmentResult("changePage", result);
        this.getParentFragmentManager().setFragmentResult("token", result);
    }
}
