package com.example.tubes2_p3b;

import java.util.List;

public class ResultRoutes {
    private List<Routes> payload;

    public ResultRoutes(List<Routes> routes){
        this.payload = routes;
    }

    public List<Routes> getPayload() {
        return payload;
    }

    public void setPayload(List<Routes> payload) {
        this.payload = payload;
    }

}

class Routes{
    String source;
    String destination;
    int fee;

    public Routes(String source, String destination, int fee){
        this.source = source;
        this.destination = destination;
        this.fee = fee;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public int getFee() {
        return fee;
    }
}
