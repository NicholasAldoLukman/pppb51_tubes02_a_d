package com.example.tubes2_p3b;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends BaseAdapter {
    private List<History> hist;
    //    private Fragment fragment;
    private Activity activity;
    ListView lv;

    public HistoryAdapter(Activity activity) {
        this.activity = activity;
        this.hist = new ArrayList<History>();
    }

    public void addLine(History hist)  {
        this.hist.add(hist);
        this.notifyDataSetChanged();
    }

    public void removeLine(int i) {
        this.hist.remove(i);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return hist.size();
    }

    @Override
    public Object getItem(int position) {
        return hist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(this.activity).inflate(R.layout.item_list_history, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        History currentHist = (History) getItem(position);

        viewHolder.route.setText(currentHist.getRoute());
        viewHolder.date.setText((CharSequence) currentHist.getDate());

        return convertView;
    }

    private class ViewHolder {
        TextView route;
        TextView date;

        public ViewHolder(View view) {
            route = (TextView)view.findViewById(R.id.tv_route);
            date = (TextView)view.findViewById(R.id.tv_date);
        }
    }
}
