package com.example.tubes2_p3b;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.example.tubes2_p3b.databinding.SeatPageSmallBinding;

import java.util.Arrays;
import java.util.List;

public class SeatPageSmallFragment extends Fragment implements View.OnTouchListener, OrderPresenter.OrderInterface, FragmentResultListener {
    SeatPageSmallBinding binding;
    private Canvas mCanvas;
    private Bitmap mBitmap;
    private GestureDetector mDetector;
    private Paint paintKosong;
    private Paint paintTeisi;
    private Paint paintDipilih;
    private Paint strokePaint;
    private Paint strokePaintDipilih;
    private Paint paintText;
    private int[] ditekan = new int[6];
    private Paint paintDiisi;
    private Paint strokePaintDiisi;
    private int[] seats;
    private OrderPresenter orderPresenter;
    private String token;
    private String course_id;

    public SeatPageSmallFragment() { }

    public static SeatPageSmallFragment newInstance(String title) {
        SeatPageSmallFragment fragment = new SeatPageSmallFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = SeatPageSmallBinding.inflate(this.getLayoutInflater());
        this.mDetector = new GestureDetector(this.getContext(), new SeatPageSmallFragment.MyCustomGestureListener());
        this.binding.ivSeat.setOnTouchListener(this);

        getParentFragmentManager().setFragmentResultListener("seats", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                seats = result.getIntArray("seats");
                initiateCanvas(kursiTersisi(seats));
            }
        });
        this.orderPresenter = new OrderPresenter(this.getActivity(), this);
        this.getParentFragmentManager().setFragmentResultListener("course_id", this, this);
        this.getParentFragmentManager().setFragmentResultListener("tokenOrder", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String token2 = result.getString("tokenOrder");
                binding.btnNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v == binding.btnNext) {
                            String seatDipesan ="";
                            for (int i = 0; i < ditekan.length; i++) {
                                if (ditekan[i] == 1) {
                                    seatDipesan += Integer.toString(i + 1) + ",";
                                }
                            }
                            seatDipesan = seatDipesan.substring(0, seatDipesan.length() - 1);
                            Log.d("seatDipesan", seatDipesan);
                            Log.d("tokenOrder", token2);

                            orderPresenter.executeOrder(course_id, seatDipesan, token2);
                            Bundle result = new Bundle();
                            result.putInt("page", 7);
                            getParentFragmentManager().setFragmentResult("changePage", result);
                        }
                    }
                });
            }
        });

        return this.binding.getRoot();
    }

    public void initiateCanvas(int[] kursiTerisi){
        // 1. Create Bitmap
        this.mBitmap = Bitmap.createBitmap(350, 500, Bitmap.Config.ARGB_8888);
        // 2. Associate the bitmap to the ImageView.
        this.binding.ivSeat.setImageBitmap(mBitmap);
        // 3. Create a Canvas with the bitmap.
        this.mCanvas = new Canvas(mBitmap);
        // background canvas
        int mColorBackground = ResourcesCompat.getColor(getResources(), R.color.canvasSeat, null);
        this.mCanvas.drawColor(mColorBackground);

        //kot
        this.paintKosong = new Paint();
        paintKosong.setColor(this.getResources().getColor(R.color.black));
        this.paintKosong.setStyle(Paint.Style.FILL);

        this.strokePaint = new Paint();
        strokePaint.setColor(this.getResources().getColor(R.color.tersedia ));
        this.paintKosong.setStyle(Paint.Style.STROKE);
        this.strokePaint.setStrokeWidth(5);

        this.paintText = new Paint();
        paintText.setColor(this.getResources().getColor(R.color.black ));
        this.paintText.setTextSize(20);

        //diisi
        this.paintDiisi = new Paint();
        paintDiisi.setColor(this.getResources().getColor(R.color.black));
        this.paintDiisi.setStyle(Paint.Style.FILL);


        strokePaintDiisi = new Paint();
        strokePaintDiisi.setColor(getResources().getColor(R.color.diisi));
        paintDiisi.setStyle(Paint.Style.STROKE);
        strokePaintDiisi.setStrokeWidth(5);


        paintDipilih = new Paint();
        paintDipilih.setColor(getResources().getColor(R.color.black));
        paintDipilih.setStyle(Paint.Style.FILL);

        strokePaintDipilih = new Paint();
        strokePaintDipilih.setColor(getResources().getColor(R.color.midnight_blue));
        paintDipilih.setStyle(Paint.Style.STROKE);
        strokePaintDipilih.setStrokeWidth(5);

        paintText = new Paint();
        paintText.setColor(getResources().getColor(R.color.black ));
        paintText.setTextSize(20);

        //supir
        mCanvas.drawRect(240,130,290,180,paintKosong);
        mCanvas.drawRect(241,131,289,179,strokePaint);
        mCanvas.drawText("S", 260, 163, paintText);

        if (kursiTerisi[0] == 0) {
            //1
            mCanvas.drawRect(60, 130, 110, 180, paintKosong);
            mCanvas.drawRect(61, 131, 109, 179, strokePaint);
            mCanvas.drawText("1", 80, 163, paintText);
        }else{
            mCanvas.drawRect(60, 130, 110, 180, paintDiisi);
            mCanvas.drawRect(61, 131, 109, 179, strokePaintDiisi);
            mCanvas.drawText("1", 80, 163, paintText);
        }

        if (kursiTerisi[1] == 0) {
            //2
            mCanvas.drawRect(60, 220, 110, 270, paintKosong);
            mCanvas.drawRect(61, 221, 109, 269, strokePaint);
            mCanvas.drawText("2", 80, 250, paintText);
        }else{
            mCanvas.drawRect(60, 220, 110, 270, paintDiisi);
            mCanvas.drawRect(61, 221, 109, 269, strokePaintDiisi);
            mCanvas.drawText("2", 80, 250, paintText);
        }


        if (kursiTerisi[2] == 0) {
            //3
            mCanvas.drawRect(240, 220, 290, 270, paintKosong);
            mCanvas.drawRect(241, 221, 289, 269, strokePaint);
            mCanvas.drawText("3", 260, 250, paintText);
        }else{
            mCanvas.drawRect(240, 220, 290, 270, paintDiisi);
            mCanvas.drawRect(241, 221, 289, 269, strokePaintDiisi);
            mCanvas.drawText("3", 260, 250, paintText);
        }

        if (kursiTerisi[3] == 0) {
            //4
            mCanvas.drawRect(60, 310, 110, 360, paintKosong);
            mCanvas.drawRect(61, 311, 109, 359, strokePaint);
            mCanvas.drawText("4", 80, 340, paintText);
        }else{
            mCanvas.drawRect(60, 310, 110, 360, paintDiisi);
            mCanvas.drawRect(61, 311, 109, 359, strokePaintDiisi);
            mCanvas.drawText("4", 80, 340, paintText);
        }

        if (kursiTerisi[4] == 0) {
            //5
            mCanvas.drawRect(150, 310, 200, 360, paintKosong);
            mCanvas.drawRect(151, 311, 199, 359, strokePaint);
            mCanvas.drawText("5", 170, 340, paintText);
        }else{
            mCanvas.drawRect(150, 310, 200, 360, paintDiisi);
            mCanvas.drawRect(151, 311, 199, 359, strokePaintDiisi);
            mCanvas.drawText("5", 170, 340, paintText);
        }

        if (kursiTerisi[5] == 0) {
            //6
            mCanvas.drawRect(240, 310, 290, 360, paintKosong);
            mCanvas.drawRect(241, 311, 289, 359, strokePaint);
            mCanvas.drawText("6", 260, 340, paintText);

        }else{
            mCanvas.drawRect(240, 310, 290, 360, paintDiisi);
            mCanvas.drawRect(241, 311, 289, 359, strokePaintDiisi);
            mCanvas.drawText("6", 260, 340, paintText);
        }
        Log.d("test", "test");
    }


    private static int[] kursiTersisi(int[] seats){
        int[] kursi = {0,0,0,0,0,0,0,0,0,0};
        if(seats == null){
            return kursi;
        }else{
            for (int i = 0; i < seats.length; i++){
                kursi[seats[i]-1] = 1;
            }
            return kursi;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent e) {
        this.mDetector.onTouchEvent(e);
        return true;
    }

    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        this.course_id = result.getString("course_id");
        this.token = result.getString("tokenOrder");
    }

    @Override
    public void callbackOrder(List<Order> order) {

    }

    private class MyCustomGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            Log.d("X=", String.valueOf(e.getX()));
            Log.d("Y=", String.valueOf(e.getY()));

            float x = e.getX()/3;
            float y = e.getY()/3;
            int[] kursi = SeatPageSmallFragment.kursiTersisi(seats);
            if(x >= 60 && x <= 100 && y >= 100 && y <= 160){//1
                if (ditekan[0] == 0 && kursi[0] == 1){
                    mCanvas.drawRect(60, 130, 110, 180, paintDiisi);
                    mCanvas.drawRect(61, 131, 109, 179, strokePaintDiisi);
                    mCanvas.drawText("1", 80, 163, paintText);
                }else if (ditekan[0] == 0 && kursi[0] == 0) {
                    mCanvas.drawRect(60,130,110,180,paintDipilih);
                    mCanvas.drawRect(61,131,109,179,strokePaintDipilih);
                    mCanvas.drawText("1", 80, 163, paintText);
                    ditekan[0] = 1;
                }else{
                    mCanvas.drawRect(60,130,110,180,paintKosong);
                    mCanvas.drawRect(61,131,109,179,strokePaint);
                    mCanvas.drawText("1", 80, 163, paintText);
                    ditekan[0] = 0;
                }
            }else if (x >= 60 && x <= 100 && y >= 180 && y <= 250){//2
                if (ditekan[1] == 0 && kursi[1] == 1){
                    mCanvas.drawRect(60, 220, 110, 270, paintDiisi);
                    mCanvas.drawRect(61, 221, 109, 269, strokePaintDiisi);
                    mCanvas.drawText("2", 80, 250, paintText);
                }else if (ditekan[1] == 0 && kursi[1] == 0){
                    mCanvas.drawRect(60,220,110,270,paintDipilih);
                    mCanvas.drawRect(61,221,109,269,strokePaintDipilih);
                    mCanvas.drawText("2", 80, 250, paintText);
                    ditekan[1] = 1;
                }else{
                    mCanvas.drawRect(60,220,110,270,paintKosong);
                    mCanvas.drawRect(61,221,109,269,strokePaint);
                    mCanvas.drawText("2", 80, 250, paintText);
                    ditekan[1] = 0;
                }
            }else if (x >= 240 && x <= 290 && y >= 180 && y <= 250){//3
                if (ditekan[2] == 0 && kursi[2] == 1){
                    mCanvas.drawRect(240, 220, 290, 270, paintDiisi);
                    mCanvas.drawRect(241, 221, 289, 269, strokePaintDiisi);
                    mCanvas.drawText("3", 260, 250, paintText);
                }else if (ditekan[2] == 0 && kursi[2] == 0) {
                    mCanvas.drawRect(240,220,290,270,paintDipilih);
                    mCanvas.drawRect(241,221,289,269,strokePaintDipilih);
                    mCanvas.drawText("3", 260, 250, paintText);
                    ditekan[2] = 1;
                }else{
                    mCanvas.drawRect(240,220,290,270,paintKosong);
                    mCanvas.drawRect(241,221,289,269,strokePaint);
                    mCanvas.drawText("3", 260, 250, paintText);
                    ditekan[2] = 0;
                }
            }else if (x >= 60 && x <= 110 && y >= 270 && y <= 320){//4
                if (ditekan[3] == 0 && kursi[3] == 1){
                    mCanvas.drawRect(60, 310, 110, 360, paintDiisi);
                    mCanvas.drawRect(61, 311, 109, 359, strokePaintDiisi);
                    mCanvas.drawText("4", 80, 340, paintText);
                }else if (ditekan[3] == 0 && kursi[3] == 0) {
                    mCanvas.drawRect(60, 310, 110, 360, paintDipilih);
                    mCanvas.drawRect(61, 311, 109, 359, strokePaintDipilih);
                    mCanvas.drawText("4", 80, 340, paintText);
                    ditekan[3] = 1;
                }else{
                    mCanvas.drawRect(60, 310, 110, 360, paintKosong);
                    mCanvas.drawRect(61, 311, 109, 359, strokePaint);
                    mCanvas.drawText("4", 80, 340, paintText);
                    ditekan[3] = 0;
                }
            }else if (x >= 150 && x <= 200 && y >= 270 && y <= 320){//5
                if (ditekan[4] == 0 && kursi[4] == 1){
                    mCanvas.drawRect(150, 310, 200, 360, paintDiisi);
                    mCanvas.drawRect(151, 311, 199, 359, strokePaintDiisi);
                    mCanvas.drawText("5", 170, 340, paintText);
                }else if (ditekan[4] == 0 && kursi[4] == 0) {
                    mCanvas.drawRect(150, 310, 200, 360, paintDipilih);
                    mCanvas.drawRect(151, 311, 199, 359, strokePaintDipilih);
                    mCanvas.drawText("5", 170, 340, paintText);
                    ditekan[4] = 1;
                }else{
                    mCanvas.drawRect(150, 310, 200, 360, paintKosong);
                    mCanvas.drawRect(151, 311, 199, 359, strokePaint);
                    mCanvas.drawText("5", 170, 340, paintText);
                    ditekan[4] = 0;
                }
            }else if (x >= 240 && x <= 290 && y >= 270 && y <= 320){//6
                if (ditekan[5] == 0 && kursi[5] == 1){
                    mCanvas.drawRect(240, 310, 290, 360, paintDiisi);
                    mCanvas.drawRect(241, 311, 289, 359, strokePaintDiisi);
                    mCanvas.drawText("6", 260, 340, paintText);
                }else if (ditekan[5] == 0 && kursi[5] == 0) {
                    mCanvas.drawRect(240, 310, 290, 360, paintDipilih);
                    mCanvas.drawRect(241, 311, 289, 359, strokePaintDipilih);
                    mCanvas.drawText("6", 260, 340, paintText);
                    ditekan[5] = 1;
                }else{
                    mCanvas.drawRect(240, 310, 290, 360, paintKosong);
                    mCanvas.drawRect(241, 311, 289, 359, strokePaint);
                    mCanvas.drawText("6", 260, 340, paintText);
                    ditekan[5] = 0;
                }
            }
            Log.d("testTekan", Arrays.toString(ditekan));
            //Log.d("ditekan", Arrays.toString(ditekan));
            Bundle result = new Bundle();
            result.putIntArray("dibeli", ditekan);

            int count = 0;
            for(int i = 0; i < ditekan.length; i++){
                if(ditekan[i] == 1){
                    count++;
                }
            }
            result.putInt("total", count*OrderPageFragment.fee);
            getParentFragmentManager().setFragmentResult("dibeli",result);
            getParentFragmentManager().setFragmentResult("total",result);




            binding.ivSeat.invalidate();
            return true;
        }
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            return true;
        }
        @Override
        public void onLongPress(MotionEvent e) {

        }
    }
}
