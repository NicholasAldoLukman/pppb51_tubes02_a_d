package com.example.tubes2_p3b;

import java.util.List;

public class ResultOrder {
    private String message;
    private List<Order> payload;

    public ResultOrder(List<Order> order){
        this.payload = order;
    }

    public List<Order> getOrder() {
        return payload;
    }

    public void setOrder(List<Order> order) {
        this.payload = order;
    }
}

class Order{
    String order_id;
    int[] seats;

    public Order(String order_id, int[] seats){
        this.order_id = order_id;
        this.seats = seats;
    }

    public String getOrder_id() {
        return order_id;
    }

    public int[] getSeats() {
        return seats;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }
}
