package com.example.tubes2_p3b;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.example.tubes2_p3b.databinding.OrderPageBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class OrderPageFragment extends Fragment implements  View.OnClickListener, RoutesPresenter.PostRoutesInterface, FragmentResultListener, CoursesPresenter.CoursesInterface {
    OrderPageBinding binding;
    RoutesPresenter routesPresenter;
    String token;
    static int fee;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SimpleDateFormat dateFormat;
    int hour, minute;
    CoursesPresenter coursesPresenter;

    public static OrderPageFragment newInstance(String title) {
        OrderPageFragment fragment = new OrderPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = OrderPageBinding.inflate(inflater, container, false);
        this.binding.btnSearch.setOnClickListener(this);
        //Spinner
        //From
        Spinner fromSpinner =(Spinner) binding.fromSpinner;
        String [] fvalues={"Bandung","Jakarta","Bekasi","Cikarang"};
        ArrayAdapter<String> fadapter =new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,fvalues);
        fadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        fromSpinner.setAdapter(fadapter);
        //To
        Spinner toSpinner =(Spinner) binding.toSpinner;
        String [] tvalues={"Bandung","Jakarta","Bekasi","Cikarang"};
        ArrayAdapter<String> tadapter =new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,tvalues);
        tadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        toSpinner.setAdapter(tadapter);

        //Size
        Spinner sizeSpinner =(Spinner) binding.sizeSpinner;
        String [] svalues={"Large","Small"};
        ArrayAdapter<String> sadapter =new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,svalues);
        sadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sizeSpinner.setAdapter(sadapter);

//        Log.d("tokenOrder", this.token);

        this.routesPresenter = new RoutesPresenter(this.getActivity(), this);
        this.coursesPresenter = new CoursesPresenter(this.getActivity(), this);
        View view = this.binding.getRoot();

        this.getParentFragmentManager().setFragmentResultListener("token", this, this);
        this.getParentFragmentManager().setFragmentResultListener("fee", this, this);


        this.binding.btnDate.setOnClickListener(this);
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        this.binding.btnTime.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) { //4
        if(v == this.binding.btnSearch) {
            if (!(this.binding.fromSpinner.getSelectedItem().equals( this.binding.toSpinner.getSelectedItem())) && !this.binding.textDate.getText().equals("") && !this.binding.textTime.getText().equals("")) {
                if (this.binding.sizeSpinner.getSelectedItem().toString() == "Large") {
                    Bundle result = new Bundle();
                    result.putInt("page", 8); //seat

                    result.putString("source", this.binding.fromSpinner.getSelectedItem().toString());
                    result.putString("destination", this.binding.toSpinner.getSelectedItem().toString());
                    result.putString("vehicle", this.binding.sizeSpinner.getSelectedItem().toString());
                    result.putString("date", binding.textDate.getText().toString());
                    result.putString("time", binding.textTime.getText().toString());

                    String time = binding.textTime.getText().toString().substring(0, 2);
                    String date = binding.textDate.getText().toString();
                    String source = binding.fromSpinner.getSelectedItem().toString();
                    String destination = binding.toSpinner.getSelectedItem().toString();
                    String vehicle = binding.sizeSpinner.getSelectedItem().toString();
                    coursesPresenter.executeCourses(source, destination, vehicle, date, time, this.token);
                    result.putString("tokenOrder", this.token);
                    Log.d("TestSourceBesar", this.binding.fromSpinner.getSelectedItem().toString());
                    this.getParentFragmentManager().setFragmentResult("source", result);
                    this.getParentFragmentManager().setFragmentResult("destination", result);
                    this.getParentFragmentManager().setFragmentResult("vehicle", result);
                    this.getParentFragmentManager().setFragmentResult("date", result);
                    this.getParentFragmentManager().setFragmentResult("time", result);
                    this.getParentFragmentManager().setFragmentResult("tokenOrder", result);
                    this.getParentFragmentManager().setFragmentResult("changePage", result);
                    Toasty.success(this.getContext(), "Searching", Toasty.LENGTH_SHORT).show();


                } else if (this.binding.sizeSpinner.getSelectedItem().toString() == "Small") {
                    Bundle result = new Bundle();
                    result.putInt("page", 5); //seat

                    result.putString("source", this.binding.fromSpinner.getSelectedItem().toString());
                    result.putString("destination", this.binding.toSpinner.getSelectedItem().toString());
                    result.putString("vehicle", this.binding.sizeSpinner.getSelectedItem().toString());
                    result.putString("date", binding.textDate.getText().toString());
                    result.putString("time", binding.textTime.getText().toString());

                    String time = binding.textTime.getText().toString().substring(0, 2);
                    String date = binding.textDate.getText().toString();
                    String source = binding.fromSpinner.getSelectedItem().toString();
                    String destination = binding.toSpinner.getSelectedItem().toString();
                    String vehicle = binding.sizeSpinner.getSelectedItem().toString();
                    coursesPresenter.executeCourses(source, destination, vehicle, date, time, this.token);
                    result.putString("tokenOrder", this.token);
                    Log.d("sourceTesting", this.binding.fromSpinner.getSelectedItem().toString());
                    this.getParentFragmentManager().setFragmentResult("source", result);
                    this.getParentFragmentManager().setFragmentResult("destination", result);
                    this.getParentFragmentManager().setFragmentResult("vehicle", result);
                    this.getParentFragmentManager().setFragmentResult("date", result);
                    this.getParentFragmentManager().setFragmentResult("time", result);
                    this.getParentFragmentManager().setFragmentResult("tokenOrder", result);
                    this.getParentFragmentManager().setFragmentResult("changePage", result);

                    Toasty.success(this.getContext(), "Searching", Toasty.LENGTH_SHORT).show();
                }
                String source = binding.fromSpinner.getSelectedItem().toString();
                String destination = binding.toSpinner.getSelectedItem().toString();
                routesPresenter.executeRoutes(source, destination, LoginPageFragment.token);
            }else{
                Toasty.error(this.getContext(),"Insert the order correctly",Toasty.LENGTH_SHORT).show();
            }
//            Bundle result =new Bundle();
//            result.putInt("page",5); //seat



            //this.getParentFragmentManager().setFragmentResult("changePage",result);
        }
        else if(v == this.binding.btnDate) {
            showDate();
        }
        else if(v == this.binding.btnTime) {
            showTime();
        }
    }

    public void showTime() {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

            TextView tvTime = binding.textTime;
            @Override
            public void onTimeSet(TimePicker timePicker, int selectHour, int selectMinute) {
                hour = selectHour;
                minute = selectMinute;
                tvTime.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minute));
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), onTimeSetListener, hour, minute, true);
        timePickerDialog.show();
    }

    public void showDate() {
        Calendar calendar = Calendar.getInstance();

        TextView tvDate = binding.textDate;
        this.datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i, i1, i2);
                tvDate.setText(dateFormat.format(newDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    @Override
    public void callback(List<Routes> routes) {
        Bundle result = new Bundle();
        result.putInt("fee",routes.get(0).getFee());
        Log.d("fee", String.valueOf(routes.get(0).getFee()));
        this.getParentFragmentManager().setFragmentResult("fee",result);
    }

    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        this.token = result.getString("token");
        this.fee = result.getInt("fee");

        Bundle res = new Bundle();
        res.putInt("feeHasil", this.fee);
        this.getParentFragmentManager().setFragmentResult("feeHasil",res);
        //String token2 = result.getString("token");
    }

    @Override
    public void callbackSeats(List<Courses> courses) {
        //Log.d("seats", String.valueOf(courses.get(0).getFee()));
        Bundle result = new Bundle();
        result.putIntArray("seats", courses.get(0).getSeats());
        result.putString("course_id", courses.get(0).getCourse_id());

        this.getParentFragmentManager().setFragmentResult("seats",result);
        this.getParentFragmentManager().setFragmentResult("course_id",result);
    }
}
