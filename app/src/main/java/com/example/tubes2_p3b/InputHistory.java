package com.example.tubes2_p3b;

public class InputHistory {
//    String route;
//    String date;
    int limit;
    int offset;

    public InputHistory(int limit, int offset) {
        this.limit = limit;
        this.offset = offset;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    //    public void setRoute(String route) {
//        this.route = route;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public String getRoute() {
//        return route;
//    }
//
//    public String getDate() {
//        return date;
//    }
}