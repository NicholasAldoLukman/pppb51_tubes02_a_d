package com.example.tubes2_p3b;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.tubes2_p3b.databinding.LandingPageBinding;

import java.util.zip.Inflater;

public class LandingPageFragment extends Fragment implements View.OnClickListener{
    LandingPageBinding binding;
    ImageView iv1;
    public static LandingPageFragment newInstance(String title) {
        LandingPageFragment fragment = new LandingPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = LandingPageBinding.inflate(inflater,container,false);
        iv1 =binding.imageview1;
        Glide.with(this)
                .load("https://media1.giphy.com/media/l0ExhgDYmserkFabm/giphy.gif?cid=ecf05e47dp5zbia75gc7f3wr7zxyuxyvxgcwfndo8n1j8wyn&rid=giphy.gif&ct=g")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(iv1);

        this.binding.btnOrderNow.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View view) { //2
        if(view == this.binding.btnOrderNow){
            Bundle result = new Bundle();
            result.putInt("page", 4);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }
}
