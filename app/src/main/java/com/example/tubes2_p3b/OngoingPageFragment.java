package com.example.tubes2_p3b;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.example.tubes2_p3b.databinding.OngoingPageBinding;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Arrays;
import java.util.List;

public class OngoingPageFragment extends Fragment implements View.OnClickListener, HistoryPresenter.HistoryInterface {
    OngoingPageBinding binding;
    String test;
    HistoryPresenter historyPresenter;

    public static OngoingPageFragment newInstance(String title) {
        OngoingPageFragment fragment = new OngoingPageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = OngoingPageBinding.inflate(inflater,container,false);
        this.binding.homeBtnnn.setOnClickListener(this);
        this.historyPresenter = new HistoryPresenter(this.getContext(), this);

        String fake="Fake encodings".trim() ;
        MultiFormatWriter writer =new MultiFormatWriter();
        try {
            BitMatrix matrix =writer.encode(fake, BarcodeFormat.QR_CODE,350,350);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap =encoder.createBitmap(matrix);
            binding.qrCode.setImageBitmap(bitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }


        this.getParentFragmentManager().setFragmentResultListener("source", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String source = result.getString("source");
                binding.source.setText(source);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("destination", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String destination = result.getString("destination");
                binding.destination.setText(destination);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("date", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String date = result.getString("date");
                binding.date.setText(date);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("time", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                String time = result.getString("time");
                binding.time.setText(time);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("dibeli", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                int[] dibeli = result.getIntArray("dibeli");
                String bangkuDipesan = "";
                int count = 0;
                for (int i = 0;i < dibeli.length; i++){
                    if(dibeli[i] == 1) {
                        bangkuDipesan += (i + 1) + ",";
                        count++;
                    }
                }

                bangkuDipesan = bangkuDipesan.substring(0, bangkuDipesan.length()-1);

                binding.seat.setText(bangkuDipesan);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("total", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                int total = result.getInt("total");
                Log.d("feeFrag",String.valueOf(total));
                binding.total.setText(String.valueOf(total));
            }
        });



        return binding.getRoot();
    }

    @Override
    public void onClick(View view) { //7
        if(view == this.binding.homeBtnnn){
            Bundle result = new Bundle();
            result.putInt("page", 2);
            this.getParentFragmentManager().setFragmentResult("changePage", result);

            //Log.d("testSource", this.test);
        }
    }

    @Override
    public void callback(List<HistoryOrder> history) {
         Bundle result = new Bundle();
         result.putString("cobaSource",history.get(0).getSource());
         this.getParentFragmentManager().setFragmentResult("cobaSource", result);
    }
}
