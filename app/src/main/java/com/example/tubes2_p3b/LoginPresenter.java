package com.example.tubes2_p3b;


import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;


public class LoginPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/authenticate";
    private Context context;
    private Gson gson;
    private PostLoginInterface IMainActivity;
    private JSONObject objJSON;

    public LoginPresenter(Context context, PostLoginInterface IMainActivity) {
        this.context = context;
        this.IMainActivity= IMainActivity;
    }

    public void execute(String username, String password) {
        gson = new Gson();
        InputLogin input = new InputLogin(username, password);
        try {
            objJSON=new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        this.callVolley(objJSON);
        //  this.callVolley(expr,precision);
    }

    private void callVolley(JSONObject toJson) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                   Context context= LoginPresenter.this.context;

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Respon", response.toString());
                        processResult(response.toString());

                        Toasty.success(context,"Login Success!",Toasty.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toasty.error(context,"Login Failed!",Toasty.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonRequest);
    }

    private void processResult(String json){
        ResultLogin result= gson.fromJson(json,ResultLogin.class);
        String token = result.getToken();
        String pesan = result.getPesan();
        this.IMainActivity.callback(token,pesan);
    }


    interface PostLoginInterface {
        public void callback(String token,String pesan);
    }
}