package com.example.tubes2_p3b;

public class InputCourses {
    String asal;
    String tujuan;
    String kendaraan;
    String date;
    String jam;

    public  InputCourses(String asal, String tujuan, String kendaraan, String date, String jam){
        this.asal = asal;
        this.tujuan = tujuan;
        this.kendaraan = kendaraan;
        this.date = date;
        this.jam = jam;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public void setKendaraan(String kendaraan) {
        this.kendaraan = kendaraan;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getAsal() {
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public String getKendaraan() {
        return kendaraan;
    }

    public String getDate() {
        return date;
    }

    public String getJam() {
        return jam;
    }
}
