package com.example.tubes2_p3b;

public class ResultLogin {
    String token;
    String pesan;

    public ResultLogin(String token, String pesan){
        this.token = token;
        this.pesan = pesan;
    }

    public String getPesan() {
        return pesan;
    }

    public String getToken() {
        return token;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
