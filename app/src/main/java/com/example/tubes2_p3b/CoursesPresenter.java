package com.example.tubes2_p3b;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CoursesPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/courses?";
    private Context context;
    private Gson gson;
    private CoursesInterface IMainActivity;
    private JSONObject objJSON;

    public CoursesPresenter(Context context, CoursesInterface IMainActivity) {
        this.context = context;
        this.IMainActivity = IMainActivity;
    }

    public void executeCourses(String source, String destination, String vehicle,String date, String hour, String token) {
        gson = new Gson();
        InputCourses input = new InputCourses(source, destination, vehicle, date, hour);
        try {
            objJSON = new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = BASE_URL + "source=" + source + "&destination=" + destination + "&vehicle=" + vehicle + "&date=" + date + "&hour=" + hour;


        //Log.d("tokenvolley", token.toString());
        this.callVolley(objJSON, token, URL);

        //  this.callVolley(expr,precision);
    }

    private void callVolley(JSONObject toJson, String token, String URL) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ResponCourses", response.toString());
                        processResult(response.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponCoursesError", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers= new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(jsonRequest);
    }

    private void processResult(String json) {
        ResultCourses result = gson.fromJson(json, ResultCourses.class);
        List<Courses> courses = result.getCourses();
        this.IMainActivity.callbackSeats(courses);
    }

    interface CoursesInterface {
        public void callbackSeats(List<Courses> courses);
    }
}