package com.example.tubes2_p3b;

public class History {
    protected String route;
    protected String date;

    public History(String route, String date) {
        this.route = route;
        this.date = date;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRoute() {
        return route;
    }

    public String getDate() {
        return date;
    }
}
