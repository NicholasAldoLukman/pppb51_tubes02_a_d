package com.example.tubes2_p3b;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class HistoryPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/orders?";
    private Context context;
    private Gson gson;
    private HistoryInterface IMainActivity;
    private JSONObject objJSON;

    public HistoryPresenter(Context context, HistoryInterface IMainActivity) {
        this.context = context;
        this.IMainActivity = IMainActivity;
    }

    public void executeHistory(int limit, int offset, String token) {
        gson = new Gson();
        InputHistory input = new InputHistory(limit, offset);
        try {
            objJSON = new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String URL = BASE_URL + "limit=" + limit + "&offset=" + offset;

        Log.d("tokenvolley", token);
        this.callVolley(objJSON, token, URL);
    }

    private void callVolley(JSONObject toJson, String token, String URL) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ResponHistory", response.toString());
                        processResult(response.toString());
                        Toasty.success(context,"History Showed!",Toasty.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ResponHistoryError", error.toString());
                Toasty.error(context,"Failed!" + error,Toasty.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers= new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(jsonRequest);
    }

    private void processResult(String json) {
        ResultHistory result = gson.fromJson(json, ResultHistory.class);
        List<HistoryOrder> history = result.getPayload();
        this.IMainActivity.callback(history);
    }

    interface HistoryInterface {
        public void callback(List<HistoryOrder> history);
    }
}
