package com.example.tubes2_p3b;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;


public class OrderPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/orders";
    private Context context;
    private Gson gson;
    private OrderInterface IMainActivity;
    private JSONObject objJSON;

    public OrderPresenter(Context context, OrderInterface IMainActivity) {
        this.context = context;
        this.IMainActivity= IMainActivity;
    }

    public void executeOrder(String course_id, String seats, String token) {
        gson = new Gson();
        InputOrder input = new InputOrder(course_id, seats);
        try {
            objJSON=new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.callVolley(objJSON, token);
        //  this.callVolley(expr,precision);
    }

    private void callVolley(JSONObject toJson, String token) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    Context context= OrderPresenter.this.context;

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ResponOrder", response.toString());
                        processResult(response.toString());
                        Toasty.success(context,"Order SENT",Toasty.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toasty.success(context,"Order FAILED",Toasty.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers= new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        requestQueue.add(jsonRequest);
    }

    private void processResult(String json){
        ResultOrder result = gson.fromJson(json,ResultOrder.class);
        List<Order> order = result.getOrder();
        this.IMainActivity.callbackOrder(order);
    }


    interface OrderInterface {
        public void callbackOrder(List<Order> order);
    }
}