package com.example.tubes2_p3b;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tubes2_p3b.databinding.LeftDrawerFragmentBinding;

public class FragmentLeftDrawer extends Fragment implements View.OnClickListener {
    LeftDrawerFragmentBinding binding;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = LeftDrawerFragmentBinding.inflate(this.getLayoutInflater());

        this.binding.hm.setOnClickListener(this);
        this.binding.hist.setOnClickListener(this);
        this.binding.Exit.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
        Bundle result = new Bundle();
        if(view == this.binding.hm){
            result.putInt("page", 2);
            getParentFragmentManager().setFragmentResult("changePage", result);
        }else if (view == this.binding.hist){
            result.putInt("page", 3);
            getParentFragmentManager().setFragmentResult("changePage", result);
        }else if(view == this.binding.Exit){
            Bundle exitApp = new Bundle();
            exitApp.putBoolean("exit", true);
            getParentFragmentManager().setFragmentResult("closeApplication",exitApp);
        }
    }
}
