package com.example.tubes2_p3b;

public class InputRoutes {
    private String asal;
    private String tujuan;
    public InputRoutes(String asal, String tujuan){
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getTujuan() {
        return tujuan;
    }

    public String getAsal() {
        return asal;
    }
}
